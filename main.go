package main

import (
	"fmt"
	"os"

	"github.com/gempir/go-twitch-irc/v3"
)

var (
	channel = "Theoriginalstove"
	account = "avocado_pit_fig_chat"
)

func main() {
	//	twitchUsername := os.Getenv("TWITCH_USERNAME")
	fmt.Println("GETTING TOKEN")
	token := os.Getenv("TWITCH_TOKEN")
	if token == "" {
		panic("unable to find TWITCH_TOKEN")
	}
	fmt.Println("Create new client")
	client := twitch.NewClient(account, "oauth:"+token)
	client.Join("theoriginalstove", "TheOriginalStove", "Saunti")

	fmt.Println("Connecting...")
	err := client.Connect()
	if err != nil {
		panic(err)
	}
	fmt.Println("joined maybe?")

	client.Say("Saunti", "Hey friend, this is a test by Stove")
	client.Say("Theoriginalstove", "testing this test message")

}
