module gitlab.com/avocagrow/avocadow-pit/figchat

go 1.18

require (
	github.com/gempir/go-twitch-irc/v3 v3.1.0
	github.com/levenlabs/go-llog v1.0.0
)

require github.com/levenlabs/errctx v1.0.0 // indirect
